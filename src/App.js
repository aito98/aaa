import React, { useState } from "react";
import axios from "axios";

function App() {

  const url = `https://api.openweathermap.org/data/2.5/weather?lat=57.1522&lon=65.5272&appid=c75700e28d18254b994d3138d603363d
`


  return (
    <div className="app">
      <div className="container">
        <div className="top">
          <div className="location">
            <p>Tyumen</p>
          </div>
          <div className="temp">
            <h1>4°C</h1>
          </div>
          <div className="description">
            <p>Clouds</p>
          </div>
        </div>
        <div className="bottom">
          <div className="feels">
            <p>2°C</p>
          </div>
          <div className="humidity">
            <p>49%</p>
          </div>
          <div className="wind">
            5 MPH
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
